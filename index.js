var express = require('express')
var app = express()

app.get('/test',(req, res)=>{
    res.send('{Response: "Hello Test"}')
})

app.get('/release',(req, res)=>{
    res.send('{Response: "Nice Gitlab Session By AKshay"}')
})


app.listen(process.env.PORT || 5000)

module.exports = app
