var supertest = require('supertest')

var app = require('../index.js')


describe('GET /test', function() {
    it("it shoud has response with Hello Test ", function(done){
        supertest(app)
          .get("/test")
          .expect('{Response: "Hello Test"}')
          .end(function(err, res){
            if (err) done(err);
            done();
          });
    });
})
